# README #
WeightCount is chain formula based weighted counting algorithm that
reduces a weighted counting problem to unweighted counting problem and
invokes SharpSAT to perform unweighted counting 

For more details on counting algorithm, please visit: http://www.cs.rice.edu/~kgm2/research.html

### Licensing ###
Please see the file `LICENSE-LGPL`.

### Contributors ###

(1) Kuldeep Meel (kuldeep@rice.edu)


### Contact ###
* Kuldeep Meel (kuldeep@rice.edu)